const express = require('express')
const bodyParser = require('body-parser')
const request = require('request')

// routers
// const categoriaRouter = require('../routes/categoria-router');

const app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

suite = (endereco, palavra) => {
    
    let y=0
    let res = []
   
    for (i=0;i< endereco.length;i++)
    {
        for (k=0;k<endereco[i].address.suite.length;k++)
        {
            if(endereco[i].address.suite[k] == palavra[0])
            {
                for(j=k;j< k+palavra.length;j++)
                {
                    if(endereco[i].address.suite[j]==palavra[j-k])
                    {
                        y++;
                    }
                    if (y==palavra.length)
                    {
                        res.push([endereco[i]])
                    }
                }
                y=0;
            }
        }
    }
    return res
}

app.get('/', (req, res) => {
    res.status(200).send(
        '<a href="/websites">WEBSITES</a>' +
        '<br>' +
        '<br>' +
        '<a href="/pessoas">PESSOAS</a>' +
        '<br>' +
        '<br>' +
        '<a href="/suite">SUITE</a>'
    )
})

app.get('/websites', (req, res) => {
    request('https://jsonplaceholder.typicode.com/users', (err, response, body) => {
        let resposta = JSON.parse(body)
        let websites = []
        resposta.forEach(el => {
            websites.push(el.website)
        });

        res.status(200).json(websites)
    })
})

app.get('/pessoas', (req, res) => {
    request('https://jsonplaceholder.typicode.com/users', (err, response, body) => {
        let resposta = JSON.parse(body)
        let obj = {}
        let pessoas = []
        for(let i = 0; i < resposta.length; i++) 
        {
            obj = {}
            obj.nome = resposta[i].name
            obj.email = resposta[i].email
            obj.empresa = resposta[i].company.name
            pessoas.push(obj)
        }

        pessoas = pessoas.sort((a, b) => {
            return (a.nome > b.nome) ? 1 : ((b.nome > a.nome) ? -1 : 0);
        })

        res.status(200).json(pessoas)
    })
})

app.get('/suite', (req, res) => {
    request('https://jsonplaceholder.typicode.com/users', (err, response, body) => {
        let resposta = JSON.parse(body)

        suites = suite(resposta, 'Suite')

        res.status(200).json(suites)
    })
})

module.exports = app;